$(document).ready(function(){
    var d = new Date();
    var y = d.getFullYear();   
    var m = d.getMonth();     
    var dt = d.getDate();
    var pYear=y; pMonth=m;
    var inputYear=0; inputMonth=0;inputDate=0, totalDays=0;
    var monthName = ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'];

    calender(y,m);

    for(var i=1970; i<=2070; i++){
        $('.year').append('<option value="'+ i +'">'+ i +'</option>');
    }
    for(var i=0; i<=11; i++){
        $('.month').append('<option value="'+ i +'">'+ monthName[i] +'</option>');
    }  
    console.log(inputYear, inputMonth, inputDate); 
    var temp = new Date(inputYear, inputMonth+1, 0);
    totalDays = temp.getDate();

    for(var i=1;i<=totalDays;i++){
        $('.date').append('<option value="'+ i +'">'+ i +'</option>');
    }

    $(document).on('click','#today',function (){
        $('.year, .month, .date').val('');
        inputYear=0; inputMonth=0;inputDate=0;
        calender(y,m);
        pMonth = m;
        pYear = y;
        
    });

    $(document).on('click','#previous',function(){
        
        // if(pYear)
        if(pMonth<=0){
            pYear--;
            pMonth=11;
        }
        else{
            pMonth--;
        } 
        calender(pYear,pMonth);
    });

    $(document).on('click','#next',function(){


        if(pMonth>=11){
            pYear++;
            pMonth=0;
        }else{
            pMonth++;
        }   
        calender(pYear,pMonth);
    }); 

    $(document).on('click','#find-date',function(){

        inputYear = $('.year').val();
        inputMonth = $('.month').val();
        inputDate = $('.date').val();
        inputMonth = parseInt(inputMonth);
        pYear = inputYear;
        pMonth = inputMonth;

        console.log(inputYear, inputMonth, inputDate);
        var temp = new Date(inputYear, inputMonth+1, 0);
        totalDays = temp.getDate();
        
        if(inputYear == null || inputMonth == null){
            alert('choose both: month & year');
        }
        else if(inputDate>totalDays){
            // $('.date').val('');
            alert('choose valid date');
        }
        else{
            calender(inputYear, inputMonth);
        }
        
    });
    
    function userInputValue(){
        inputYear = $('.year').val();
        inputMonth = $('.month').val();
        inputDate = $('.date').val();

    }

    function calender(userYear, userMonth){

        var year = userYear;
        var month = userMonth;
        var count = 0;              
        var currentMonth = new Date(year, month);
        var firstDayOfCurrentMonth = currentMonth.getDay();

        $('#current-month').html(monthName[month] + ' - ' +year);
        
        

        var x = new Date(year, month+1, 0);
        var numberOfDaysInMonth = x.getDate();
        console.log(numberOfDaysInMonth);
        
        var table = $('tbody').html('');
        for(var i=0; i<=5; i++){
            var row = '<tr>'
            for(var j=0; j<=6; j++){
                row += '<td>' + '' + '</td>';        
            }
            row += '</tr>'
            table.append(row);
        }
        
        for(var j=0;j<=6;j++){
            if(j==firstDayOfCurrentMonth){
                var flag = j;
                count =1;
                for(var k=0; k<numberOfDaysInMonth; k++){
                    $('tbody td').eq(flag).html(count);
                    if(count==dt && month==m && year==y){
                        $('tbody td').eq(flag).css('background-color','grey');
                    }
                    if(count==inputDate && month==inputMonth && year==inputYear){
                        $('tbody td').eq(flag).css('background-color','lime');
                    }
                    flag++;
                    count++;
                }
            }
           
            
            
        }  
    
    }
});